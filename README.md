# Task

To create an application which calls for GitHub API and returns info on repositories: fullName, description, 
cloneUrl stars and createdAt. If there are some results returned, then the RequestDetail data should persisted to 
the database with the following information: owner, repositoryName, timestamp and IPAddress of the caller. Moreover,
application should contain an endpoint to return all the persisted data.

# Used Libraries and Tools

* Maven
* SpringBoot
* Mockito and JUnit
* Jackson
* MapStruct
* Lombok
* H2/MySQL

# Solution description

Before start, one should make at least mvn package so that the MapStruct generates all the required code.

Application endpoints:

http://localhost:8080/repositories/{owner}/{repositoryName}

Makes a call to GitHub API to return the required data about the GitHub repository. Jackson maps the response to the 
desired object. If the call is successful, there is a call for saving info about the request in the database.  

http://localhost:8080/repositories/

Returns all the persisted data in the database about the requests.

# Production-ready

I provided the properties file to repository for the easiness of the assessor to get up and running, however, these
files should not be saved to the repository in real-life application, as they contain relevant data.

The property files that are dependent upon environment on which the application is running. It is realized via Maven 
and Spring profiles. In order to make the environment-specific build, one should run the mvn command with the 
-P<profileName> property, for instance for dev it would be -Pdev. The default profile is set to dev.

Make sure that the production configuration in properties conforms the real needs, that is there a user with the
corresponding password for MySQL database. If there is no such user, one can either change the property file or
use the following command line commands to add another one (root should replaced with user with admin rights):

mysql --user=root mysql
	CREATE USER 'admin'@'localhost' IDENTIFIED BY 'password';
	GRANT ALL PRIVILEGES ON *.* TO 'admin'@'localhost' WITH GRANT OPTION;
	
Obviously, in the POM file, from the version should removed SNAPSHOT for production. If there would be more modules,
it could be possible to make via maven-versions-plugin to change all the poms at once.
	
#Sidenote

As for the requirement on 20 requests per second, with GitHub API it is hardly doable. I tested the clean API via 
Postman and calling the clean API lasted for ~185ms, what gives at best 5 requests per second. I tried to filter 
the call to the GitHub API and limit the size of the response - other APIs had such possibility to add query
parameters in the URL like ?fields=field1, field2, or some similar syntax, however on the GitHub API this does not
affect the results. I was thinking also of wrapping the API with GraphQL, however this would also not give the expected
result, as it would still call the flat REST.