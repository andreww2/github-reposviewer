package com.wrzosek.githubreposviewer.controllers;

import com.wrzosek.githubreposviewer.api.model.RequestDetailDTO;
import com.wrzosek.githubreposviewer.domain.GitHubRepoData;
import com.wrzosek.githubreposviewer.exceptions.NoRepoFoundException;
import com.wrzosek.githubreposviewer.services.GitHubRepoDataService;
import com.wrzosek.githubreposviewer.services.RequestDetailService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



public class GitHubControllerTest {

    private final String FULL_NAME = "fullname";
    private final String DESCRIPTION = "description";
    private final String CLONE_URL = "cloneurl";
    private final int STARS = 255;
    private final String ENDPOINT_URL = "owner/repository";


    @Mock
    private GitHubRepoData gitHubRepoDataMock;

    @Mock
    private GitHubRepoDataService gitHubRepoDataServiceMock;

    @Mock
    private RequestDetailService requestDetailServiceMock;

    @InjectMocks
    private GitHubController gitHubController;

    private MockMvc mockMvc;

    @Before
    public void setup() {

        //Initialization
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(gitHubController)
                .setControllerAdvice(new NoRepoFoundExceptionHandler())
                .build();

        gitHubRepoDataMock = new GitHubRepoData();
        gitHubRepoDataMock.setFullName(FULL_NAME);
        gitHubRepoDataMock.setDescription(DESCRIPTION);
        gitHubRepoDataMock.setCloneUrl(CLONE_URL);
        gitHubRepoDataMock.setStars(STARS);
    }

    @Test
    public void getGitHubRepoDataTest() throws Exception {

        //Given
        when(gitHubRepoDataServiceMock.getGitHubRepoData(anyString(), anyString())).thenReturn(gitHubRepoDataMock);

        //Perform
        mockMvc.perform(get(GitHubController.BASE_URL + ENDPOINT_URL)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.fullName", equalTo(FULL_NAME)))
                .andExpect(jsonPath("$.description", equalTo(DESCRIPTION)))
                .andExpect(jsonPath("$.cloneUrl", equalTo(CLONE_URL)))
                .andExpect(jsonPath("$.stars", equalTo(STARS)));
    }

    @Test
    public void repoNotFoundTest() throws Exception {

        //Given
        when(gitHubRepoDataServiceMock.getGitHubRepoData(anyString(), anyString()))
                .thenThrow(NoRepoFoundException.class);

        //Perform
        mockMvc.perform(get(GitHubController.BASE_URL + ENDPOINT_URL)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getRequestDetailsTest() throws Exception {

        //Preparation of data
        RequestDetailDTO requestDetailDTO = new RequestDetailDTO();
        RequestDetailDTO requestDetailDTO2 = new RequestDetailDTO();

        //Given
        when(requestDetailServiceMock.getRequestDetails()).thenReturn(Arrays.asList(requestDetailDTO, requestDetailDTO2));

        //Perform
        mockMvc.perform(get(GitHubController.BASE_URL)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.requestDetails", hasSize(2)));
    }
}