package com.wrzosek.githubreposviewer.services.impl;

import com.wrzosek.githubreposviewer.domain.GitHubRepoData;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class GitHubRepoDataServiceImplTest {

    private final String FULL_NAME = "mark/facebook";
    private final String DESCRIPTION = "repo desc";
    private final String CLONE_URL = "http://example-repo.git";
    private final int STARS = 12;
    //22.10.2016
    private final Date CREATED_AT = new Date(1477094400000L);

    @InjectMocks
    private GitHubRepoDataServiceImpl gitHubRepoDataService;

    @Mock
    private RestTemplate restTemplate;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        gitHubRepoDataService = new GitHubRepoDataServiceImpl("https://api.example.com/repos/", restTemplate);
    }

    @Test
    public void getGitHubRepoDataTest() {

        //Preparation of data
        GitHubRepoData gitHubRepoData = new GitHubRepoData();
        gitHubRepoData.setFullName(FULL_NAME);
        gitHubRepoData.setDescription(DESCRIPTION);
        gitHubRepoData.setCloneUrl(CLONE_URL);
        gitHubRepoData.setStars(STARS);
        gitHubRepoData.setCreatedAt(CREATED_AT);

        //Given
        when(restTemplate.getForObject(anyString(), any(), anyString(), anyString()))
                .thenReturn(gitHubRepoData);
        GitHubRepoData found = gitHubRepoDataService
                .getGitHubRepoData(FULL_NAME.split("/")[0], FULL_NAME.split("/")[1]);

        //Then
        assertEquals(gitHubRepoData, found);
    }
}
