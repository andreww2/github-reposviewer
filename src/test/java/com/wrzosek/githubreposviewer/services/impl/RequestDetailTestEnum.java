package com.wrzosek.githubreposviewer.services.impl;

import java.util.Date;

public enum RequestDetailTestEnum {

    INSTANCE;

    public static final String OWNER = "owner";
    public static final String REPOSITORY_NAME = "repo";
    public static final String IP_ADDRESS = "1.2.3.4";
    //22.10.2016
    public static final Date TIMESTAMP = new Date(1477094400000L);

}