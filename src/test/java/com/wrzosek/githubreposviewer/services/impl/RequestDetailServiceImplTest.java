package com.wrzosek.githubreposviewer.services.impl;

import com.wrzosek.githubreposviewer.api.mapper.RequestDetailMapper;
import com.wrzosek.githubreposviewer.api.model.RequestDetailDTO;
import com.wrzosek.githubreposviewer.domain.RequestDetail;
import com.wrzosek.githubreposviewer.exceptions.NoRepoFoundException;
import com.wrzosek.githubreposviewer.repositories.RequestDetailRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static com.wrzosek.githubreposviewer.services.impl.RequestDetailTestEnum.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class RequestDetailServiceImplTest {

    private RequestDetailServiceImpl requestDetailService;

    RequestDetail requestDetail;

    @Mock
    private RequestDetailRepository requestDetailRepository;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        requestDetailService = new RequestDetailServiceImpl(requestDetailRepository, RequestDetailMapper.INSTANCE);

        requestDetail = new RequestDetail.RequestDetailBuilder()
                .withOwner(OWNER)
                .withRepositoryName(REPOSITORY_NAME)
                .withIpAddress(IP_ADDRESS)
                .withTimestamp(TIMESTAMP)
                .build();
    }

    @Test
    public void getRequestDetailsTest() {

        List<RequestDetail> requestDetails = Arrays.asList(new RequestDetail(), new RequestDetail());
        when(requestDetailRepository.getRequestDetails())
                .thenReturn(requestDetails);

        List<RequestDetailDTO> received = requestDetailService.getRequestDetails();

        assertEquals(2, received.size());
    }

    @Test
    public void saveRequestDetailTest() {

        when(requestDetailRepository.save(requestDetail)).thenReturn(requestDetail);

        RequestDetail received = requestDetailService.saveRequestDetail(OWNER, REPOSITORY_NAME, IP_ADDRESS, TIMESTAMP);

        assertEquals(OWNER, received.getOwner());
        assertEquals(REPOSITORY_NAME, received.getRepositoryName());
        assertEquals(IP_ADDRESS, received.getIpAddress());
        assertEquals(TIMESTAMP, received.getTimestamp());
    }

    @Test(expected = NoRepoFoundException.class)
    public void getGitHubRepoDataTest() {

        when(requestDetailRepository.save(requestDetail)).thenThrow(NoRepoFoundException.class);

        requestDetailService.saveRequestDetail(OWNER, REPOSITORY_NAME, IP_ADDRESS, TIMESTAMP);
    }
}
