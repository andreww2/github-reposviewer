package com.wrzosek.githubreposviewer.services.impl;

import com.wrzosek.githubreposviewer.api.mapper.RequestDetailMapper;
import com.wrzosek.githubreposviewer.api.model.RequestDetailDTO;
import com.wrzosek.githubreposviewer.repositories.RequestDetailRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static com.wrzosek.githubreposviewer.services.impl.RequestDetailTestEnum.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RequestDetailServiceImplIT {

    @Autowired
    private RequestDetailRepository requestDetailRepository;

    private RequestDetailServiceImpl requestDetailService;

    private List<RequestDetailDTO> foundRequestDetailDTOs;

    @Before
    public void setup() {

        requestDetailService = new RequestDetailServiceImpl(
                requestDetailRepository, RequestDetailMapper.INSTANCE);

        requestDetailService.saveRequestDetail(OWNER, REPOSITORY_NAME, IP_ADDRESS, TIMESTAMP);
        requestDetailService.saveRequestDetail(OWNER, REPOSITORY_NAME, IP_ADDRESS, TIMESTAMP);
    }

    @Test
    public void getRequestDetailsIT() {

        foundRequestDetailDTOs = requestDetailService.getRequestDetails();

        assertEquals(OWNER, foundRequestDetailDTOs.get(0).getOwner());
        assertEquals(REPOSITORY_NAME, foundRequestDetailDTOs.get(0).getRepositoryName());
        assertEquals(IP_ADDRESS, foundRequestDetailDTOs.get(0).getIpAddress());
        assertEquals(TIMESTAMP, foundRequestDetailDTOs.get(0).getTimestamp());
        assertEquals(2, foundRequestDetailDTOs.size());
    }
}
