package com.wrzosek.githubreposviewer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GithubRepsApplication {

    public static void main(String[] args) {
        SpringApplication.run(GithubRepsApplication.class, args);
    }
}
