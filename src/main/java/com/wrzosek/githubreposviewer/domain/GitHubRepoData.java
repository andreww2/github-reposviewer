package com.wrzosek.githubreposviewer.domain;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GitHubRepoData {

    @JsonAlias("full_name")
    private String fullName;

    private String description;

    @JsonAlias("clone_url")
    private String cloneUrl;

    @JsonAlias("stargazers_count")
    private int stars;

    @JsonAlias("created_at")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createdAt;
}
