package com.wrzosek.githubreposviewer.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
@JsonIgnoreProperties("private")
public class RequestDetail {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String owner;

    @Column
    private String repositoryName;

    //I use java.util.Date, because of lack of compatibility with LocalDateTime.
    @JsonFormat(timezone = "GMT+1")
    @Column
    private Date timestamp;

    @Column
    private String ipAddress;

    public static class RequestDetailBuilder {

        private String owner;
        private String repositoryName;
        private Date timestamp;
        private String ipAddress;

        public RequestDetailBuilder withOwner(String owner) {
            this.owner = owner;
            return this;
        }

        public RequestDetailBuilder withRepositoryName(String name) {
            this.repositoryName = name;
            return this;
        }

        public RequestDetailBuilder withTimestamp(Date timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public RequestDetailBuilder withIpAddress(String ipAddress) {
            this.ipAddress = ipAddress;
            return this;
        }

        public RequestDetail build() {

            RequestDetail requestDetail = new RequestDetail();
            requestDetail.owner = this.owner;
            requestDetail.repositoryName = this.repositoryName;
            requestDetail.timestamp = this.timestamp;
            requestDetail.ipAddress = this.ipAddress;

            return requestDetail;
        }
    }
}
