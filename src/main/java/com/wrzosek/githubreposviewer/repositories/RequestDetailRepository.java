package com.wrzosek.githubreposviewer.repositories;

import com.wrzosek.githubreposviewer.domain.RequestDetail;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RequestDetailRepository extends CrudRepository<RequestDetail, Long> {
    @Query("select r from RequestDetail r")
    List<RequestDetail> getRequestDetails();
}
