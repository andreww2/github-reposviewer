package com.wrzosek.githubreposviewer.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Repo not found")
public class NoRepoFoundException extends RuntimeException {
}
