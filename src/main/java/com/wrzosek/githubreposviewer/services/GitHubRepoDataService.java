package com.wrzosek.githubreposviewer.services;

import com.wrzosek.githubreposviewer.domain.GitHubRepoData;

public interface GitHubRepoDataService {

    GitHubRepoData getGitHubRepoData(String owner, String repositoryName);

}
