package com.wrzosek.githubreposviewer.services;


import com.wrzosek.githubreposviewer.api.model.RequestDetailDTO;

import java.util.Date;
import java.util.List;

public interface RequestDetailService {

    List<RequestDetailDTO> getRequestDetails();

    void setFieldsAndSaveRequestDetail(String owner, String repositoryName, Date timestamp);
}
