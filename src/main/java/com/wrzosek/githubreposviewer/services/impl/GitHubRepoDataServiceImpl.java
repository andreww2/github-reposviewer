package com.wrzosek.githubreposviewer.services.impl;

import com.wrzosek.githubreposviewer.domain.GitHubRepoData;
import com.wrzosek.githubreposviewer.exceptions.NoRepoFoundException;
import com.wrzosek.githubreposviewer.services.GitHubRepoDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class GitHubRepoDataServiceImpl implements GitHubRepoDataService {

    private final String url;

    private RestTemplate restTemplate;

    @Autowired
    public GitHubRepoDataServiceImpl(@Value("${github.repo.url}") String url,
                                     RestTemplate restTemplate) {
        this.url = url;
        this.restTemplate = restTemplate;
    }

    @Override
    public GitHubRepoData getGitHubRepoData(String owner, String repositoryName) {
        GitHubRepoData gitHubRepoData;

        try {
            gitHubRepoData = restTemplate.getForObject(url, GitHubRepoData.class, owner, repositoryName);
        } catch (HttpClientErrorException e) {
            throw new NoRepoFoundException();
        }
        return gitHubRepoData;
    }
}
