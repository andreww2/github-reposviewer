package com.wrzosek.githubreposviewer.services.impl;

import com.wrzosek.githubreposviewer.api.mapper.RequestDetailMapper;
import com.wrzosek.githubreposviewer.api.model.RequestDetailDTO;
import com.wrzosek.githubreposviewer.domain.RequestDetail;
import com.wrzosek.githubreposviewer.repositories.RequestDetailRepository;
import com.wrzosek.githubreposviewer.services.RequestDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RequestDetailServiceImpl implements RequestDetailService {

    private final RequestDetailRepository requestDetailRepository;
    private final RequestDetailMapper requestDetailMapper;


    @Autowired
    public RequestDetailServiceImpl(RequestDetailRepository requestDetailRepository, RequestDetailMapper requestDetailMapper) {

        this.requestDetailRepository = requestDetailRepository;
        this.requestDetailMapper = requestDetailMapper;
    }

    @Override
    public List<RequestDetailDTO> getRequestDetails() {
        return requestDetailRepository
                .getRequestDetails()
                .stream()
                .map(requestDetailMapper::requestDetailToRequestDetailDTO)
                .collect(Collectors.toList());
    }

    protected RequestDetail saveRequestDetail(String owner, String repositoryName, String ipAddress, Date timestamp) {

        RequestDetail requestDetail = new RequestDetail.RequestDetailBuilder()
                .withOwner(owner)
                .withRepositoryName(repositoryName)
                .withTimestamp(timestamp)
                .withIpAddress(ipAddress)
                .build();

        return requestDetailRepository.save(requestDetail);
    }

    @Override
    public void setFieldsAndSaveRequestDetail(String owner, String repositoryName, Date timestamp) {

              String ipAddress = null;

        try {
            ipAddress = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        saveRequestDetail(owner, repositoryName, ipAddress, timestamp);
    }

}
