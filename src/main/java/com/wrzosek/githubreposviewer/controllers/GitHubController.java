package com.wrzosek.githubreposviewer.controllers;

import com.wrzosek.githubreposviewer.api.model.RequestDetailListDTO;
import com.wrzosek.githubreposviewer.domain.GitHubRepoData;
import com.wrzosek.githubreposviewer.services.GitHubRepoDataService;
import com.wrzosek.githubreposviewer.services.RequestDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class GitHubController {

    public static final String BASE_URL = "/repositories/";

    private final RequestDetailService requestDetailService;
    private final GitHubRepoDataService gitHubRepoDataService;

    @Autowired
    public GitHubController(RequestDetailService requestDetailService, GitHubRepoDataService gitHubRepoDataService) {
        this.requestDetailService = requestDetailService;
        this.gitHubRepoDataService = gitHubRepoDataService;
    }

    @GetMapping(BASE_URL + "{owner}/{repositoryName}")
    public GitHubRepoData getGitHubRepoData(@PathVariable String owner, @PathVariable String repositoryName) {

        Date timestamp = new Date(System.currentTimeMillis());

        GitHubRepoData gitHubRepoData = gitHubRepoDataService.getGitHubRepoData(owner, repositoryName);

        requestDetailService.setFieldsAndSaveRequestDetail(owner, repositoryName, timestamp);
        return gitHubRepoData;
    }

    @GetMapping(BASE_URL)
    public RequestDetailListDTO getRequestDetails() {
        return new RequestDetailListDTO(requestDetailService.getRequestDetails());
    }
}
