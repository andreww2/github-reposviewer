package com.wrzosek.githubreposviewer.controllers;

import com.wrzosek.githubreposviewer.exceptions.NoRepoFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class NoRepoFoundExceptionHandler extends ResponseEntityExceptionHandler {

        @ExceptionHandler(NoRepoFoundException.class)
        public ResponseEntity<Object> handleNotFoundException(){

            return new ResponseEntity<>("Repo not found", new HttpHeaders(), HttpStatus.NOT_FOUND);
        }
}
