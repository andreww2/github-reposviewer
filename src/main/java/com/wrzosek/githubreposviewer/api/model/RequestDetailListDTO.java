package com.wrzosek.githubreposviewer.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class RequestDetailListDTO {

    List<RequestDetailDTO> requestDetails;
}
