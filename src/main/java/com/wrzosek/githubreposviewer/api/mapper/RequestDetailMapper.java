package com.wrzosek.githubreposviewer.api.mapper;

import com.wrzosek.githubreposviewer.api.model.RequestDetailDTO;
import com.wrzosek.githubreposviewer.domain.RequestDetail;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface RequestDetailMapper {

    RequestDetailMapper INSTANCE = Mappers.getMapper(RequestDetailMapper.class);

    RequestDetailDTO requestDetailToRequestDetailDTO(RequestDetail requestDetail);

    RequestDetail requestDetailDTOToRequestDetail(RequestDetailDTO requestDetailDTO);
}
